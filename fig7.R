options(crayon.enabled = FALSE)
library(tidyverse)
library(DiceKriging)
read_time_results_exp <- function(myfile){
to.read = file(myfile, "rb")

ite <- readBin(to.read, double(), endian = "little")

all_ite <- c()
all_theta1 <- c()
all_theta2 <- c()
all_theta3 <- c()
all_logli <- c()
all_time <- c()
all_ext_timewo <- c()
all_ext_time <- c()
all_overhead <- c()
i <- 0
while (length(ite)>0){
all_ite <- c(all_ite, i)
i <- i + 1
#Theta 1
all_theta1 <- c(all_theta1, readBin(to.read, double(), endian = "little"))
#Theta 2
all_theta2 <- c(all_theta2, readBin(to.read, double(), endian = "little"))
#Theta 3
all_theta3 <- c(all_theta3, readBin(to.read, double(), endian = "little"))
#Logli
all_logli <- c(all_logli, readBin(to.read, double(), endian = "little"))
#Time
all_time <- c(all_time, readBin(to.read, double(), endian = "little"))
#Ext Time wo over
all_ext_timewo <- c(all_ext_timewo, readBin(to.read, double(), endian = "little"))
#Ext time
all_ext_time <- c(all_ext_time, readBin(to.read, double(), endian = "little"))
#Over
all_overhead <- c(all_overhead, readBin(to.read, double(), endian = "little"))
#Ite again
ite <- readBin(to.read, double(), endian = "little")
}
close(to.read)
return(data.frame(all_ite=all_ite, all_theta1=all_theta1, all_theta2=all_theta2, all_theta3=all_theta3,
         all_logli=all_logli, all_time=all_time, all_ext_timewo=all_ext_timewo, all_ext_time=all_ext_time,
         all_overhead=all_overhead))

}

read_time_results_exp2 <- function(myfile){
to.read = file(myfile, "rb")

ite <- readBin(to.read, double(), endian = "little")

all_ite <- c()
all_theta1 <- c()
all_theta2 <- c()
all_theta3 <- c()
all_logli <- c()
all_time <- c()
all_ext_timewo <- c()
all_ext_time <- c()
all_overhead <- c()
i <- 0
while (length(ite)>0){
all_ite <- c(all_ite, i)
i <- i + 1
#Theta 1
all_theta1 <- c(all_theta1, readBin(to.read, integer(), endian = "little"))
#Theta 2
all_theta2 <- c(all_theta2, readBin(to.read, double(), endian = "little"))
#Theta 3
all_theta3 <- c(all_theta3, readBin(to.read, double(), endian = "little"))
#Logli
all_logli <- c(all_logli, readBin(to.read, double(), endian = "little"))
#Time
all_time <- c(all_time, readBin(to.read, double(), endian = "little"))
#Ext Time wo over
all_ext_timewo <- c(all_ext_timewo, readBin(to.read, double(), endian = "little"))
#Ext time
all_ext_time <- c(all_ext_time, readBin(to.read, double(), endian = "little"))
#Over
all_overhead <- c(all_overhead, readBin(to.read, double(), endian = "little"))
#Ite again
ite <- readBin(to.read, double(), endian = "little")
}

return(data.frame(all_ite=all_ite, all_theta1=all_theta1, all_theta2=all_theta2, all_theta3=all_theta3,
         all_logli=all_logli, all_time=all_time, all_ext_timewo=all_ext_timewo, all_ext_time=all_ext_time,
         all_overhead=all_overhead))

}

read_time_results_all <- function(myfile){
to.read = file(myfile, "rb")

ite <- readBin(to.read, double(), endian = "little")
return(ite)
}
read_all_data_times <- function(base, opt, case_lear, rep){
all_info_lear <- list()

for(i in seq(1, rep)){

all_info_lear[[i]] <- read_time_results_exp2(paste0(base, i-1, opt, case_lear, i, "/time_results_exp"))

}

lapply(all_info_lear, function(x){return(max(x$all_ite))}) %>% unlist() %>% min() -> min_ite

all_info <- data.frame(case=c(), all_ite=c(), all_theta1=c(), all_theta2=c(), all_theta3=c(),
     all_logli=c(), all_time=c(), all_ext_timewo=c(), all_ext_time=c(), all_overhead=c())
exec <- 0
for( c in all_info_lear){
  all_info <- bind_rows(all_info, c[1:min_ite, ] %>% mutate(case=exec))
  exec <- exec + 1
}


return(all_info)

}

all_data <- read_all_data_times("traces/exp_exa_learning_6che_6chi_2cho_dice_revise.",
                     ".1.1.1.1.1.1.101.",
                     "0.0.mult_dice_101_6che_6chi_2cho.6.0.00.",
                     10)

all_data %>% group_by(all_ite) %>%
summarize(mean=mean(all_overhead), sd=sd(all_overhead)) %>%
ggplot() +
geom_point(aes(x=all_ite, y=mean)) +
#geom_jitter(data=all_data, aes(x=all_ite, y=all_overhead)) +
#geom_smooth(aes(x=all_ite, y=mean)) +
geom_errorbar(aes(x=all_ite, ymin=mean-sd, ymax=mean+sd), width=.5,
                 position=position_dodge(.9)) +
ylim(0, NA) +
theme_bw(base_size=20) +
xlab("Iteration") +
         theme(plot.margin = margin(t = 0, b = 0, l = 0, r = 0, unit='cm')) +
         theme(legend.margin = margin(t = 0.2, b = 0, l = 0, r = 0, unit='cm')) + 
ylab("Time [s]") -> acc_overhead
ggsave("./img/overhead_rev.png", plot=acc_overhead, width=600, height=250, units = "px", dpi=72)
